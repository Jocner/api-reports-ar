import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Voluntario } from 'src/domain/models/Voluntario.schema';
import { VoluntarioDTO } from 'src/domain/dto/voluntario.dto';

@Injectable()
export class VoluntarioService {
    constructor(@InjectModel(Voluntario.name) private voluntarioModel: Model<Voluntario>){}

    async create(voluntarioDTO: VoluntarioDTO): Promise<any> {
        try {

            const data = await this.voluntarioModel.create(voluntarioDTO)

            return data

        } catch(err) {
            console.log({err});
        }
    }

    async show(): Promise<any> {

        const data = await this.voluntarioModel.find();

        const resul = data ? data : 'Fail Data';
            
        return resul  
    
    }
}
