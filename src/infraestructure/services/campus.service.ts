import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Campu } from '../../domain/models/Campus.schema'
import { CampusDTO } from 'src/domain/dto/campus.dto';



@Injectable()
export class CampusService {
    constructor(@InjectModel(Campu.name) private campuModel: Model<Campu>) {}

        async create(campusDTO :CampusDTO): Promise<any> {
            try {

                const data = await this.campuModel.create(campusDTO)

                return data

            } catch(err) {
            console.log({err});
            }
        }

        async show(): Promise<any> {

            const data = await this.campuModel.find();

            const resul = data ? data : 'Fail Data';
                
            return resul
            
        
        }

        async update(id, campusDTO :CampusDTO): Promise<any> {

            try {

                const data = await this.campuModel.findByIdAndUpdate(id, campusDTO, { new: true });

                console.log(data)

                const resul = data ? data : 'Fail Data';
                    
                return resul
            
            } catch(err) {
                
                console.log(err);
            }
        
        }
}
