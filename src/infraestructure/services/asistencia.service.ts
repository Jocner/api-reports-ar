import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { AsistenciaDTO } from 'src/domain/dto/asistencia.dto';
import { Asistencia } from 'src/domain/models/Asistencia.schema';

@Injectable()
export class AsistenciaService {
    constructor(@InjectModel(Asistencia.name) private asistenciaModel: Model<Asistencia>) {}

    async create(asistenciaDTO: AsistenciaDTO): Promise<any> {
        try {

            const data = await this.asistenciaModel.create(asistenciaDTO)

            return data

        } catch(err) {
            console.log({err});
        }
    }

    async show(): Promise<any> {

        const data = await this.asistenciaModel.find();

        const resul = data ? data : 'Fail Data';
            
        return resul  
    
    }

}
