import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Stand } from 'src/domain/models/Stand.schema';
import { StandDTO } from 'src/domain/dto/stand.dto';

@Injectable()
export class StandService {
    constructor(@InjectModel(Stand.name) private standModel: Model<Stand>) {}

    async create(standDTO: StandDTO): Promise<any> {
        try {

            const data = await this.standModel.create(standDTO)

            return data

        } catch(err) {
            console.log({err});
        }
    }

    async show(): Promise<any> {

        const data = await this.standModel.find();

        const resul = data ? data : 'Fail Data';
            
        return resul
        
    
    }


}
