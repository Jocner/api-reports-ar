import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CampusController } from './controllers/campus.controller';
import { CampusService } from './services/campus.service';
import { Campu, CampuSchema} from '../domain/models/Campus.schema'
import { Stand, StandSchema} from '../domain/models/Stand.schema'
import { StandController } from './controllers/stand.controller';
import { StandService } from './services/stand.service';
import { AsistenciaService } from './services/asistencia.service';
import { AsistenciaController } from './controllers/asistencia.controller';
import { Asistencia, AsistenciaSchema } from 'src/domain/models/Asistencia.schema';
import { VoluntarioController } from './controllers/voluntario.controller';
import { VoluntarioService } from './services/voluntario.service';
import { Voluntario, VoluntarioSchema } from 'src/domain/models/Voluntario.schema';

@Module({
    imports: [MongooseModule.forFeature([{ name: Campu.name, schema: CampuSchema }, { name: Stand.name, schema: StandSchema }, {name: Asistencia.name, schema: AsistenciaSchema}, {name: Voluntario.name, schema: VoluntarioSchema}])],
    controllers: [CampusController, StandController, AsistenciaController, VoluntarioController],
    providers: [CampusService, StandService, AsistenciaService, VoluntarioService]
})
export class InfraestructureModule {}
