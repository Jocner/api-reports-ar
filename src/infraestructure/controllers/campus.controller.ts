import { Body, Controller, Get, Param, Post, Put } from '@nestjs/common';
import { CampusService } from '../services/campus.service'
import { CampusDTO } from 'src/domain/dto/campus.dto';

@Controller('campus')
export class CampusController {
    constructor( private readonly campusService: CampusService ) {}

    @Post('/')
    async new(@Body() campusDTO :CampusDTO): Promise<any> {

        try {

            const service = await this.campusService.create(campusDTO);

            return service;
            
        } catch (err) {
            console.log({err});
        }

    }
    
    @Get('/')
    async all(): Promise<any> {

        try {

            const service = await this.campusService.show();
            console.log(service)
            return service
         
        } catch(e) {
            console.log(e);
        }
        
    }

    @Put('/:id')
    async up(@Body() campusDTO: CampusDTO, @Param('id') id: string): Promise<any> {

        try {

            const service = await this.campusService.update(id, campusDTO);

            return service
         
        } catch(e) {
            console.log(e);
        }
        
    }
}
