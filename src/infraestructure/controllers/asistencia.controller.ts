import { Body, Controller, Get, Post } from '@nestjs/common';
import { AsistenciaService } from '../services/asistencia.service';
import { AsistenciaDTO } from 'src/domain/dto/asistencia.dto';

@Controller('asistencia')
export class AsistenciaController {
    constructor(private readonly asistenciaService : AsistenciaService){}

    @Post('/')
    async new(@Body() asistenciaDTO : AsistenciaDTO): Promise<any> {

        try {

            const service = await this.asistenciaService.create(asistenciaDTO);

            return service;
            
        } catch (err) {
            console.log({err});
        }

    }

    @Get('/')
    async all(): Promise<any> {

        try {

            const service = await this.asistenciaService.show();

            return service
         
        } catch(e) {
            console.log(e);
        }
        
    }
}
