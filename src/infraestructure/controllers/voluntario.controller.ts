import { Body, Controller, Get, Post } from '@nestjs/common';
import { VoluntarioService } from '../services/voluntario.service';
import { VoluntarioDTO } from 'src/domain/dto/voluntario.dto';

@Controller('voluntario')
export class VoluntarioController {
    constructor(private readonly voluntarioService : VoluntarioService){}

    @Post('/')
    async new(@Body() voluntarioDTO : VoluntarioDTO): Promise<any> {

        try {

            const service = await this.voluntarioService.create(voluntarioDTO);

            return service;
            
        } catch (err) {
            console.log({err});
        }

    }

    @Get('/')
    async all(): Promise<any> {

        try {

            const service = await this.voluntarioService.show();

            return service
         
        } catch(e) {
            console.log(e);
        }
        
    }
}
