import { Body, Controller, Get, Post } from '@nestjs/common';
import { StandService } from '../services/stand.service';
import { StandDTO } from 'src/domain/dto/stand.dto';

@Controller('stand')
export class StandController {

    constructor( private readonly standService: StandService ) {}

    @Post('/')
    async new(@Body() standDTO :StandDTO): Promise<any> {

        try {

            const service = await this.standService.create(standDTO);

            return service;
            
        } catch (err) {
            console.log({err});
        }

    }

    @Get('/')
    async all(): Promise<any> {

        try {

            const service = await this.standService.show();

            return service
         
        } catch(e) {
            console.log(e);
        }
        
    }

}
