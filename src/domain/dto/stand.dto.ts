import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsArray } from 'class-validator';

export class StandDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  info: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  oracion: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  recursos: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  amorporcasa: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  proyectoeducativo: string;
}