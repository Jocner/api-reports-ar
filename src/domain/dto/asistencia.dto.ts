import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty } from 'class-validator';

export class AsistenciaDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  adulto: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  tweens: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  kids: string;

}
