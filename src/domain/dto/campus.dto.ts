import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsArray } from 'class-validator';

export class CampusDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  encuentro: object[];

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  pastores: string;

  @ApiProperty()
  @IsString()
  modalidad: string;
}
