import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsNotEmpty, IsArray } from 'class-validator';

export class VoluntarioDTO {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  servicio: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsArray()
  tecnica: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  kids: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  tweens: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  worship: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  cocina: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  redesocial: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  seguridad: string;
}




