import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type AsistenciaDocument = HydratedDocument<Asistencia>;

@Schema()
export class Asistencia {
  @Prop({ required: true })
  adulto: string;

  @Prop( { required: true } )
  tweens: string;

  @Prop({ required: true })
  kids: string;

}

export const AsistenciaSchema = SchemaFactory.createForClass(Asistencia);