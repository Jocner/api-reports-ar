import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type VoluntarioDocument = HydratedDocument<Voluntario>;

@Schema()
export class Voluntario {
  @Prop({ required: true })
  servicio: string;

  @Prop( { required: true } )
  tecnica: string;

  @Prop({ required: true })
  kids: string;

  @Prop({ required: true })
  tweens: string;

  @Prop( { required: true } )
  worship: string;

  @Prop({ required: true })
  cocina: string;

  @Prop( { required: true } )
  redesocial: string;

  @Prop({ required: true })
  seguridad: string;

}

export const VoluntarioSchema = SchemaFactory.createForClass(Voluntario);