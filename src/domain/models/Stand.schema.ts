import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type StandDocument = HydratedDocument<Stand>;

@Schema()
export class Stand {
  @Prop({ required: true })
  info: string;

  @Prop( { required: true } )
  oracion: string;

  @Prop({ required: true })
  recursos: string;

  @Prop({ required: true })
  amorporcasa: string;

  @Prop({ required: true })
  proyectoeducativo: string;
}

export const StandSchema = SchemaFactory.createForClass(Stand);