import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export type CatDocument = HydratedDocument<Campu>;

@Schema()
export class Campu {
  @Prop({ required: true })
  name: string;

  @Prop( [Object] )
  encuentro: object[];

  @Prop({ required: true })
  pastores: string;

  @Prop({ required: false })
  modalidad: string;
}

export const CampuSchema = SchemaFactory.createForClass(Campu);